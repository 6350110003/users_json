// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

List<Welcome> welcomeFromJson(String str) => List<Welcome>.from(json.decode(str).map((x) => Welcome.fromJson(x)));

String welcomeToJson(List<Welcome> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Welcome {
  Welcome({
    required this.username,
    required this.email,
    required this.urlAvatar,
  });

  final String username;
  final String email;
  final String urlAvatar;

  factory Welcome.fromJson(Map<String, dynamic> json) => Welcome(
    username: json["username"],
    email: json["email"],
    urlAvatar: json["urlAvatar"],
  );

  Map<String, dynamic> toJson() => {
    "username": username,
    "email": email,
    "urlAvatar": urlAvatar,
  };
}
